# Reto - Desarrollo CLM - Dev: Edgar Castro

Realizar "npm install" para instalar todas las dependencias necesarias, 
luego iniciar el proyecto con "node app" o "nodemon app" dentro del directorio del proyecto.

La documentacion necesaria para el consumo de los servicios se encuentra en el 
archivo swagger.yaml. En caso de que no puedan ver la documentacion de swagger,
recomiendo copiar el contenido del mismo y pegarlo en "https://editor.swagger.io/".

Para cualquier duda o pregunta no duden en contactarme
dev.edgarcastro@gmail.com
+584122682800