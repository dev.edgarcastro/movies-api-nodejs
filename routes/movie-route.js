const {Router} = require('express');
const {validateQuery, validateBody} = require('../middleware/movie');
const { moviesGet, requestMoviesGet, requestMoviesPost } =  require('../controllers/movies-controller');
const {MovieValidatorSchema, RequestGetValidatorSchema, RequestPostValidatorSchema}  = require('../validations/movie-validator');
const router = Router();

router.get('/search', validateQuery(MovieValidatorSchema), moviesGet);
router.get('/request',validateQuery(RequestGetValidatorSchema),requestMoviesGet );
router.post('/request',validateBody(RequestPostValidatorSchema),  requestMoviesPost);




module.exports = router;