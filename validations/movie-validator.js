const Joi = require('joi');

const MovieValidatorSchema = Joi.object({
    title: Joi.string().required(),
    year: Joi.string()

});

const RequestGetValidatorSchema = Joi.object({
    limit: Joi.number()
});
// movie, find, replace
const RequestPostValidatorSchema = Joi.object({
    movie: Joi.string().required(),
    find: Joi.string().required(),
    replace: Joi.string().required()
});

module.exports = {
    MovieValidatorSchema,
    RequestGetValidatorSchema,
    RequestPostValidatorSchema
};
