const {response, request} = require('express');
const logger = require('../utils/logger');
const axios = require('axios');
const Movie = require('../models/movie-schema');
const moviesUrl = 'http://www.omdbapi.com';
const apiKey = '5f2cbbe9';

const moviesGet = (req = request, res = response) => {
    const {title, year = ''} = req.query;


    axios.get(`${moviesUrl}/?t=${title}&y=${year}&apikey=${apiKey}`)
        .then(async response => {
            const movie = new Movie(response.data);

            await movie.save().then(() => logger.info('save in database'));

            res.json({
                movie
            });
        })
        .catch(error => {
            logger.error(error);
            if (error.code === 11000) {
                res.json({
                    msg_error: 'the film exists in database',
                });
                return;
            }
            res.json({
                msg_error: error,
            });
        });
};

const requestMoviesGet = async (req = request, res = response) => {

    const {limit = 5} = req.query;
    const movies = await Movie.find()
        .limit(Number(limit));
    res.json({
        movies
    });
};

const requestMoviesPost = async (req = request, res = response) => {

    const {movie, find, replace} = req.body;
    const movieResponse = await Movie.findOne({Title: {$regex: new RegExp(movie, "i")}});
    const moviePlotMod = movieResponse.Plot.split(find).join(replace);
    res.json({
        movieTitle: movieResponse.Title,
        moviePlotOriginal: movieResponse.Plot,
        moviePlotMod: moviePlotMod
    });
};

module.exports = {
    moviesGet,
    requestMoviesGet,
    requestMoviesPost

};