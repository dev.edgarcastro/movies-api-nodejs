const logger = require('../utils/logger');

const validateQuery = (schema) => {
    return async (req, res, next) => {
        try {
            await schema.validateAsync(req.query);
            next();
        } catch (e) {
            res.send(e.message);
            logger.error(e.message);
        }
    }
};
const validateBody = (schema) => {
    return async (req, res, next) => {
        try {
            await schema.validateAsync(req.body);
            next();
        } catch (e) {
            res.send(e.message);
            logger.error(e.message);
        }
    }
};

module.exports = {
    validateQuery, validateBody
};