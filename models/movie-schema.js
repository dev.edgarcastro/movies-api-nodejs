const {Schema, model} = require('mongoose');

const MovieSchema = Schema({
    Title: {
        type: String,
        required: [true, 'title is required'],
        unique: true
    },
    Year: {
        type: String,
        required: [true, 'year is required']
    },
    Released: {
        type: String,
        required: [true, 'release is required']
    },
    Genre: {
        type: String,
        required: [true, 'genre is required']
    },
    Director: {
        type: String,
        required: [true, 'director is required']
    },
    Actors: {
        type: String,
        required: [true, 'actors is required']
    },
    Plot: {
        type: String,
        required: [true, 'plot is required']
    },
    Ratings: [{
        Source: {
            type: String,
            required: [true, 'Source is required']
        },
        Value: {
            type: String,
            required: [true, 'Value is required']
        }

    }]
});
MovieSchema.method('toJSON', function () {
    const {__v, _id, ...object} = this.toObject();
    return object;
});
module.exports = model('Movie', MovieSchema);