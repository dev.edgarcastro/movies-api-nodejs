const express = require('express');
const logger = require('../utils/logger');
const { dbConnection } = require('../database/config');


class Server {

    constructor() {
        this.app = express();
        this.port = process.env.Port;
        this.moviesPath = '/movies';
        // Connect Database
        this.connectDB();

        this.middlewares();

        this.routes();
       //  this.callApi();
    }

    middlewares() {
        this.app.use(express.static('public'));

        this.app.use(express.json());

    }
    async connectDB() {
        await dbConnection();
    }

    routes() {

        this.app.use(this.moviesPath, require('../routes/movie-route'));
    }

    listen() {
        this.app.listen(this.port, () => {
            logger.info(`Server running on port: ${this.port}`);
        });
    }


}

module.exports = Server;