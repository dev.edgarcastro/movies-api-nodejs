const mongoose = require('mongoose');
const logger = require('../utils/logger');

const dbConnection = async () => {


    try {

        await mongoose.connect(process.env.MONGODB_CNN,);
        logger.info('Database Online');

    } catch (e) {

        logger.error('Database Online');
        throw new Error('Unable to initialize database');
    }

};

module.exports = {
    dbConnection
};